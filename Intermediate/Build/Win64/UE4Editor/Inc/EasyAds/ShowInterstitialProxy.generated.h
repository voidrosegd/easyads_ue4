// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UShowInterstitialProxy;
#ifdef EASYADS_ShowInterstitialProxy_generated_h
#error "ShowInterstitialProxy.generated.h already included, missing '#pragma once' in ShowInterstitialProxy.h"
#endif
#define EASYADS_ShowInterstitialProxy_generated_h

#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_SPARSE_DATA
#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execShowInterstitial);


#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execShowInterstitial);


#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShowInterstitialProxy(); \
	friend struct Z_Construct_UClass_UShowInterstitialProxy_Statics; \
public: \
	DECLARE_CLASS(UShowInterstitialProxy, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyAds"), NO_API) \
	DECLARE_SERIALIZER(UShowInterstitialProxy)


#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUShowInterstitialProxy(); \
	friend struct Z_Construct_UClass_UShowInterstitialProxy_Statics; \
public: \
	DECLARE_CLASS(UShowInterstitialProxy, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EasyAds"), NO_API) \
	DECLARE_SERIALIZER(UShowInterstitialProxy)


#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShowInterstitialProxy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShowInterstitialProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShowInterstitialProxy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShowInterstitialProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShowInterstitialProxy(UShowInterstitialProxy&&); \
	NO_API UShowInterstitialProxy(const UShowInterstitialProxy&); \
public:


#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShowInterstitialProxy(UShowInterstitialProxy&&); \
	NO_API UShowInterstitialProxy(const UShowInterstitialProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShowInterstitialProxy); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShowInterstitialProxy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UShowInterstitialProxy)


#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_PRIVATE_PROPERTY_OFFSET
#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_18_PROLOG
#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_PRIVATE_PROPERTY_OFFSET \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_SPARSE_DATA \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_RPC_WRAPPERS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_INCLASS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_PRIVATE_PROPERTY_OFFSET \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_SPARSE_DATA \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_INCLASS_NO_PURE_DECLS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EASYADS_API UClass* StaticClass<class UShowInterstitialProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID runner_Plugins_EasyAdsPlugin_Source_EasyAds_Public_ShowInterstitialProxy_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
