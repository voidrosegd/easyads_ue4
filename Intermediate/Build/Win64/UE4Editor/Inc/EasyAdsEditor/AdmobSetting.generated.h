// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef EASYADSEDITOR_AdmobSetting_generated_h
#error "AdmobSetting.generated.h already included, missing '#pragma once' in AdmobSetting.h"
#endif
#define EASYADSEDITOR_AdmobSetting_generated_h

#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_SPARSE_DATA
#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_RPC_WRAPPERS
#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAdmobSetting(); \
	friend struct Z_Construct_UClass_UAdmobSetting_Statics; \
public: \
	DECLARE_CLASS(UAdmobSetting, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EasyAdsEditor"), NO_API) \
	DECLARE_SERIALIZER(UAdmobSetting) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUAdmobSetting(); \
	friend struct Z_Construct_UClass_UAdmobSetting_Statics; \
public: \
	DECLARE_CLASS(UAdmobSetting, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EasyAdsEditor"), NO_API) \
	DECLARE_SERIALIZER(UAdmobSetting) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAdmobSetting(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAdmobSetting) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAdmobSetting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAdmobSetting); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAdmobSetting(UAdmobSetting&&); \
	NO_API UAdmobSetting(const UAdmobSetting&); \
public:


#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAdmobSetting(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAdmobSetting(UAdmobSetting&&); \
	NO_API UAdmobSetting(const UAdmobSetting&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAdmobSetting); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAdmobSetting); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAdmobSetting)


#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AndroidAppId() { return STRUCT_OFFSET(UAdmobSetting, AndroidAppId); } \
	FORCEINLINE static uint32 __PPO__AndroidBannerUnit() { return STRUCT_OFFSET(UAdmobSetting, AndroidBannerUnit); } \
	FORCEINLINE static uint32 __PPO__AndroidInterstitialUnit() { return STRUCT_OFFSET(UAdmobSetting, AndroidInterstitialUnit); } \
	FORCEINLINE static uint32 __PPO__AndroidRewardedVideoAdUnit() { return STRUCT_OFFSET(UAdmobSetting, AndroidRewardedVideoAdUnit); } \
	FORCEINLINE static uint32 __PPO__AndroidTestDevice() { return STRUCT_OFFSET(UAdmobSetting, AndroidTestDevice); } \
	FORCEINLINE static uint32 __PPO__EnableAndroidTestSuite() { return STRUCT_OFFSET(UAdmobSetting, EnableAndroidTestSuite); } \
	FORCEINLINE static uint32 __PPO__DisableAndroidUnity() { return STRUCT_OFFSET(UAdmobSetting, DisableAndroidUnity); } \
	FORCEINLINE static uint32 __PPO__DisableAndroidVungle() { return STRUCT_OFFSET(UAdmobSetting, DisableAndroidVungle); } \
	FORCEINLINE static uint32 __PPO__DisableAndroidChartboost() { return STRUCT_OFFSET(UAdmobSetting, DisableAndroidChartboost); } \
	FORCEINLINE static uint32 __PPO__IOSAppId() { return STRUCT_OFFSET(UAdmobSetting, IOSAppId); } \
	FORCEINLINE static uint32 __PPO__IOSBannerUnit() { return STRUCT_OFFSET(UAdmobSetting, IOSBannerUnit); } \
	FORCEINLINE static uint32 __PPO__IOSInterstitialUnit() { return STRUCT_OFFSET(UAdmobSetting, IOSInterstitialUnit); } \
	FORCEINLINE static uint32 __PPO__IOSRewardedVideoAdUnit() { return STRUCT_OFFSET(UAdmobSetting, IOSRewardedVideoAdUnit); } \
	FORCEINLINE static uint32 __PPO__IOSTestDevice() { return STRUCT_OFFSET(UAdmobSetting, IOSTestDevice); }


#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_16_PROLOG
#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_PRIVATE_PROPERTY_OFFSET \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_SPARSE_DATA \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_RPC_WRAPPERS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_INCLASS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_PRIVATE_PROPERTY_OFFSET \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_SPARSE_DATA \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_INCLASS_NO_PURE_DECLS \
	runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EASYADSEDITOR_API UClass* StaticClass<class UAdmobSetting>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID runner_Plugins_EasyAdsPlugin_Source_EasyAdsEditor_Private_AdmobSetting_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
