// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EasyAdsEditor/Private/AdmobSetting.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAdmobSetting() {}
// Cross Module References
	EASYADSEDITOR_API UClass* Z_Construct_UClass_UAdmobSetting_NoRegister();
	EASYADSEDITOR_API UClass* Z_Construct_UClass_UAdmobSetting();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_EasyAdsEditor();
// End Cross Module References
	void UAdmobSetting::StaticRegisterNativesUAdmobSetting()
	{
	}
	UClass* Z_Construct_UClass_UAdmobSetting_NoRegister()
	{
		return UAdmobSetting::StaticClass();
	}
	struct Z_Construct_UClass_UAdmobSetting_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AndroidAppId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AndroidAppId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AndroidBannerUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AndroidBannerUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AndroidInterstitialUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AndroidInterstitialUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AndroidRewardedVideoAdUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AndroidRewardedVideoAdUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AndroidTestDevice_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AndroidTestDevice;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnableAndroidTestSuite_MetaData[];
#endif
		static void NewProp_EnableAndroidTestSuite_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_EnableAndroidTestSuite;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisableAndroidUnity_MetaData[];
#endif
		static void NewProp_DisableAndroidUnity_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DisableAndroidUnity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisableAndroidVungle_MetaData[];
#endif
		static void NewProp_DisableAndroidVungle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DisableAndroidVungle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisableAndroidChartboost_MetaData[];
#endif
		static void NewProp_DisableAndroidChartboost_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DisableAndroidChartboost;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IOSAppId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IOSAppId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IOSBannerUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IOSBannerUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IOSInterstitialUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IOSInterstitialUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IOSRewardedVideoAdUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IOSRewardedVideoAdUnit;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IOSTestDevice_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_IOSTestDevice;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAdmobSetting_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_EasyAdsEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "AdmobSetting.h" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidAppId_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Android AppID" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidAppId = { "AndroidAppId", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, AndroidAppId), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidAppId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidAppId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidBannerUnit_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Android Banner AdUnit" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidBannerUnit = { "AndroidBannerUnit", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, AndroidBannerUnit), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidBannerUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidBannerUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidInterstitialUnit_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Android Interstitial AdUnit" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidInterstitialUnit = { "AndroidInterstitialUnit", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, AndroidInterstitialUnit), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidInterstitialUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidInterstitialUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidRewardedVideoAdUnit_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Android RewardVideo AdUnit" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidRewardedVideoAdUnit = { "AndroidRewardedVideoAdUnit", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, AndroidRewardedVideoAdUnit), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidRewardedVideoAdUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidRewardedVideoAdUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidTestDevice_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Android TestDevice" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidTestDevice = { "AndroidTestDevice", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, AndroidTestDevice), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidTestDevice_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidTestDevice_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_EnableAndroidTestSuite_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Enable Mediation TestSute" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
		{ "ToolTip", "this require minSdkVersion 16, else will package fail" },
	};
#endif
	void Z_Construct_UClass_UAdmobSetting_Statics::NewProp_EnableAndroidTestSuite_SetBit(void* Obj)
	{
		((UAdmobSetting*)Obj)->EnableAndroidTestSuite = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_EnableAndroidTestSuite = { "EnableAndroidTestSuite", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdmobSetting), &Z_Construct_UClass_UAdmobSetting_Statics::NewProp_EnableAndroidTestSuite_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_EnableAndroidTestSuite_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_EnableAndroidTestSuite_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidUnity_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Disable Unity" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	void Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidUnity_SetBit(void* Obj)
	{
		((UAdmobSetting*)Obj)->DisableAndroidUnity = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidUnity = { "DisableAndroidUnity", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdmobSetting), &Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidUnity_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidUnity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidUnity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidVungle_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Disable Vungle" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	void Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidVungle_SetBit(void* Obj)
	{
		((UAdmobSetting*)Obj)->DisableAndroidVungle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidVungle = { "DisableAndroidVungle", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdmobSetting), &Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidVungle_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidVungle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidVungle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidChartboost_MetaData[] = {
		{ "Category", "Android" },
		{ "DisplayName", "Disable Chartboost" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	void Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidChartboost_SetBit(void* Obj)
	{
		((UAdmobSetting*)Obj)->DisableAndroidChartboost = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidChartboost = { "DisableAndroidChartboost", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UAdmobSetting), &Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidChartboost_SetBit, METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidChartboost_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidChartboost_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSAppId_MetaData[] = {
		{ "Category", "IOS" },
		{ "DisplayName", "IOS AppID" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSAppId = { "IOSAppId", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, IOSAppId), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSAppId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSAppId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSBannerUnit_MetaData[] = {
		{ "Category", "IOS" },
		{ "DisplayName", "IOS Banner AdUnit" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSBannerUnit = { "IOSBannerUnit", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, IOSBannerUnit), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSBannerUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSBannerUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSInterstitialUnit_MetaData[] = {
		{ "Category", "IOS" },
		{ "DisplayName", "IOS Interstitial AdUnit" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSInterstitialUnit = { "IOSInterstitialUnit", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, IOSInterstitialUnit), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSInterstitialUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSInterstitialUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSRewardedVideoAdUnit_MetaData[] = {
		{ "Category", "IOS" },
		{ "DisplayName", "IOS RewardVideo AdUnit" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSRewardedVideoAdUnit = { "IOSRewardedVideoAdUnit", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, IOSRewardedVideoAdUnit), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSRewardedVideoAdUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSRewardedVideoAdUnit_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSTestDevice_MetaData[] = {
		{ "Category", "IOS" },
		{ "DisplayName", "IOS TestDevice" },
		{ "ModuleRelativePath", "Private/AdmobSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSTestDevice = { "IOSTestDevice", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAdmobSetting, IOSTestDevice), METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSTestDevice_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSTestDevice_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAdmobSetting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidAppId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidBannerUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidInterstitialUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidRewardedVideoAdUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_AndroidTestDevice,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_EnableAndroidTestSuite,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidUnity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidVungle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_DisableAndroidChartboost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSAppId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSBannerUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSInterstitialUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSRewardedVideoAdUnit,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAdmobSetting_Statics::NewProp_IOSTestDevice,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAdmobSetting_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAdmobSetting>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAdmobSetting_Statics::ClassParams = {
		&UAdmobSetting::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAdmobSetting_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::PropPointers),
		0,
		0x000000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UAdmobSetting_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAdmobSetting_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAdmobSetting()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAdmobSetting_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAdmobSetting, 2696563874);
	template<> EASYADSEDITOR_API UClass* StaticClass<UAdmobSetting>()
	{
		return UAdmobSetting::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAdmobSetting(Z_Construct_UClass_UAdmobSetting, &UAdmobSetting::StaticClass, TEXT("/Script/EasyAdsEditor"), TEXT("UAdmobSetting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAdmobSetting);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
