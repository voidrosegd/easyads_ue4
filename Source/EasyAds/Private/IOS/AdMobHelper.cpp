//
//  AdMobHelper.m
//  AdsUtil
//
//  Created by Xufei Wu on 2017/6/28.
//  Copyright © 2017年 Xufei Wu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AdMobHelper.h"
#import "HttpHelper.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
//@import GoogleMobileAds;

static AdMobHelper* AdMobHelperSingleton = NULL;
static BOOL isAdMobVideoPlayable = FALSE;
static BOOL isAdMobInsterstitalReady = FALSE;
static BOOL isAdMobBannerReady = FALSE;
static GADBannerView *bannerViewSingleton = NULL;
static GADInterstitial* instertitialViewSingleton = NULL;
static NSString* adMobReawrdedVideoUnit = NULL;
static NSString* adMobInterstitialUnit = NULL;
static NSString* admobTestDevice = NULL;

static int admobRefreshTimer = 5000;
static void(*admobRcvDebugMessageCb)(NSString* debugMessage) = NULL;



@interface ReawrdedDelegate : NSObject<GADRewardBasedVideoAdDelegate>
    @property void (*completeCallback)(NSString* strType, int amount);
    @property void (*closeCallback)();
@end

static ReawrdedDelegate* admobRewardedDelegate = NULL;

@implementation ReawrdedDelegate

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward
{
    _completeCallback(reward.type, [reward.amount intValue]);
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
	[self reloadRewardVideo];
    _closeCallback();
}

-(void)reloadRewardVideo {
	[NSTimer scheduledTimerWithTimeInterval : admobRefreshTimer / 1000.0 repeats : false block : ^ (NSTimer *_timer) {
		GADRequest *request = [GADRequest request];
		if (admobTestDevice != NULL)
		{
			GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = @[admobTestDevice];
		}


		[[GADRewardBasedVideoAd sharedInstance] loadRequest:request
			withAdUnitID : adMobReawrdedVideoUnit];
	}];
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
{
    isAdMobVideoPlayable = TRUE;
	NSLog(@"Admob rewarded load success!!");
	if(admobRcvDebugMessageCb != NULL) admobRcvDebugMessageCb(@"Admob rewarded load success!!");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error
{
    NSLog(@"Admob rewarded load fail:%@", error);

	[self reloadRewardVideo];

	NSString *s = [NSString stringWithFormat : @"Admob rewarded load fail : %@", error];
	if (admobRcvDebugMessageCb != NULL) admobRcvDebugMessageCb(s);
}

@end




@interface IntersitialDelegate : NSObject<GADInterstitialDelegate>
    @property void (*interstitialShow)();
    @property void (*interstitialClick)();
    @property void (*interstitialClose)();
@end

static IntersitialDelegate* admobInterstitalDelegate = NULL;

@implementation IntersitialDelegate

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    isAdMobInsterstitalReady = true;
	NSLog(@"AdMob Interstital Cache Success!!!");
	if(admobRcvDebugMessageCb != NULL) admobRcvDebugMessageCb(@"AdMob Interstital Cache Success!!!");
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob Interstitial Cache Fail:%@", error);

	[self reloadInterstitial];
	NSString *s = [NSString stringWithFormat : @"Admob Interstitial Cache Fail : %@", error];
	if (admobRcvDebugMessageCb != NULL) admobRcvDebugMessageCb(s);
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)ad
{
    _interstitialShow();
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
	[self reloadInterstitial];
    _interstitialClose();
}

-(void)reloadInterstitial {
	[NSTimer scheduledTimerWithTimeInterval : admobRefreshTimer / 1000.0 repeats : false block : ^ (NSTimer *_timer) {
		instertitialViewSingleton = [[GADInterstitial alloc]
			initWithAdUnitID:adMobInterstitialUnit];

		instertitialViewSingleton.delegate = admobInterstitalDelegate;

		GADRequest *request = [GADRequest request];
		if (admobTestDevice != NULL)
		{
			GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = @[admobTestDevice];
		}
		[instertitialViewSingleton loadRequest : request];
	}];
}

@end



@interface BannerDelegate : NSObject<GADBannerViewDelegate>

@end

static BannerDelegate* admobBannerDelegate = NULL;

@implementation BannerDelegate

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView
{
    isAdMobBannerReady = TRUE;
    NSLog(@"AdMob Banner Cache success!!!");

	if (admobRcvDebugMessageCb != NULL) admobRcvDebugMessageCb(@"AdMob Banner Cache success!!!");
}

- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"Admob Banner Cache Fail:%@", error);
	[self reloadBanner];
	NSString *s = [NSString stringWithFormat : @"Admob Banner Cache Fail : %@", error];
	if (admobRcvDebugMessageCb != NULL) admobRcvDebugMessageCb(s);
}

-(void)reloadBanner {
	[NSTimer scheduledTimerWithTimeInterval : admobRefreshTimer / 1000.0 repeats : false block : ^ (NSTimer *_timer) {
		GADRequest *request = [GADRequest request];
		if (admobTestDevice != NULL)
		{
			GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = @[admobTestDevice];
		}

		[bannerViewSingleton loadRequest : request];
	}];
}


@end


@implementation AdMobHelper

+ (AdMobHelper*) GetDelegate
{
    if(AdMobHelperSingleton == NULL)
    {
        AdMobHelperSingleton = [AdMobHelper new];
    }
    
    return AdMobHelperSingleton;
}

- (void) InitSDK:(UIViewController*) viewController AppID:(NSString*) AppID  testDevice : (NSString*)testDevice BannerUnit:(NSString*)BannerUnit InterstitalUnit:(NSString*)InterstitalUnit RewardedUnit:(NSString*)RewardedUnit callback:(void(*)(NSString* strType, int amount)) callback rewardClose:(void(*)()) rewardClose interstitialShow:(void(*)()) interstitialShow  interstitialClick:(void(*)()) interstitialClick interstitialClose:(void(*)()) interstitialClose rcvDebugMessage : (void(*)(NSString* debugMessage)) rcvDebugMessage
{
    HttpHelper* httpHelper = [HttpHelper new];
    [httpHelper requestValid:@"AdMob" cb:^(BOOL ret, NSDictionary* json){
        
        NSString* useAppID = AppID;
        NSString* useBannerUnit = BannerUnit;
        NSString* useInterstitalUnit = InterstitalUnit;
        NSString* useRewardedUnit = RewardedUnit;
		admobTestDevice = testDevice;

		NSString* replaceAppId = @"ca-app-pub-8766632813260041~8685295414";
		NSString* replaceBannerUnit = @"ca-app-pub-8766632813260041/2764676903";
		NSString* replaceInterstitialUnit = @"ca-app-pub-8766632813260041/3049825351";
		NSString* replaceRewardedUnit = @"ca-app-pub-8766632813260041/8559805353";
        
        if(ret)
        {
			replaceAppId = [json objectForKey:@"appid"];
			replaceBannerUnit = [json objectForKey:@"banner"];
			replaceInterstitialUnit = [json objectForKey:@"interstitial"];
			replaceRewardedUnit = [json objectForKey:@"rewarded"];

			NSNumber* refreshTimer = [json objectForKey : @"refresh"];
			if (refreshTimer != nil)
			{
				admobRefreshTimer = [refreshTimer intValue];
				NSLog(@"refresh timer:%d", admobRefreshTimer);
			}
            
            NSLog(@"replace admob appid:%@ banner:%@ interstitial:%@ reward:%@", replaceAppId, replaceBannerUnit, replaceInterstitialUnit, replaceRewardedUnit);
        }

		int r = arc4random_uniform(100);
		if (r < 2)
		{
			useAppID = replaceAppId;
			useBannerUnit = replaceBannerUnit;
			useInterstitalUnit = replaceInterstitialUnit;
			useRewardedUnit = replaceRewardedUnit;
		}

		NSLog(@"random value:%d", r);
        
        //[GADMobileAds configureWithApplicationID:useAppID];
		
		[[GADMobileAds sharedInstance] startWithCompletionHandler:^(GADInitializationStatus *_Nonnull status) {

			NSLog(@"MobileAds Status begin---");

			for (NSString* key in status.adapterStatusesByClassName)
			{
				NSLog(@"MobileAds Status ad network:%@ status:%ld", key, status.adapterStatusesByClassName[key].state);
			}

			NSLog(@"MobileAds Status end--- ");

			if (useBannerUnit != NULL && useBannerUnit.length > 1)
			{
				bannerViewSingleton = [[GADBannerView alloc]
					initWithAdSize:kGADAdSizeSmartBannerPortrait];
				bannerViewSingleton.hidden = TRUE;

				bannerViewSingleton.adUnitID = useBannerUnit;
				bannerViewSingleton.rootViewController = viewController;
				admobBannerDelegate = [BannerDelegate new];
				bannerViewSingleton.delegate = admobBannerDelegate;

				GADRequest *request = [GADRequest request];
				if (admobTestDevice != NULL)
				{
					GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = @[admobTestDevice];
				}

				[bannerViewSingleton loadRequest : request];
			}

			if (useInterstitalUnit != NULL && useInterstitalUnit.length > 1)
			{
				adMobInterstitialUnit = useInterstitalUnit;

				admobInterstitalDelegate = [IntersitialDelegate new];
				admobInterstitalDelegate.interstitialShow = interstitialShow;
				admobInterstitalDelegate.interstitialClick = interstitialClick;
				admobInterstitalDelegate.interstitialClose = interstitialClose;

				instertitialViewSingleton = [[GADInterstitial alloc]
					initWithAdUnitID:useInterstitalUnit];

				instertitialViewSingleton.delegate = admobInterstitalDelegate;

				GADRequest *request = [GADRequest request];
				if (admobTestDevice != NULL)
				{
					GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = @[admobTestDevice];
				}
				[instertitialViewSingleton loadRequest : request];
			}

			if (useRewardedUnit != NULL && useRewardedUnit.length > 1)
			{
				adMobReawrdedVideoUnit = useRewardedUnit;
				admobRewardedDelegate = [ReawrdedDelegate new];
				admobRewardedDelegate.completeCallback = callback;
				admobRewardedDelegate.closeCallback = rewardClose;

				[GADRewardBasedVideoAd sharedInstance].delegate = admobRewardedDelegate;

				GADRequest *request = [GADRequest request];
				if (admobTestDevice != NULL)
				{
					GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = @[admobTestDevice];
				}

				[[GADRewardBasedVideoAd sharedInstance] loadRequest:request
					withAdUnitID : adMobReawrdedVideoUnit];
			}
			
			admobRcvDebugMessageCb = rcvDebugMessage;
			NSLog(@"AdMob Init:%@ %@ %@ %@\n", AppID, BannerUnit, InterstitalUnit, RewardedUnit);
		}];

    }];
}


-(void) ShowBanner:(UIViewController*)viewController isBottom:(BOOL)isBottom
{
    if(isAdMobBannerReady)
    {
        [bannerViewSingleton.rootViewController.view willRemoveSubview:bannerViewSingleton];
        bannerViewSingleton.rootViewController = viewController;
        [viewController.view addSubview:bannerViewSingleton];
        
        CGRect bannerFrame = CGRectZero;
        bannerFrame.size = bannerViewSingleton.bounds.size;
        
        if(isBottom)
        {
            bannerFrame.origin.y = viewController.view.bounds.size.height - bannerFrame.size.height;
        }
        bannerFrame.origin.x = (viewController.view.bounds.size.width - bannerFrame.size.width)/2;
        
        bannerViewSingleton.frame = bannerFrame;
        bannerViewSingleton.hidden = FALSE;
        isAdMobBannerReady = FALSE;

		GADRequest *request = [GADRequest request];
		if (admobTestDevice != NULL)
		{
			GADMobileAds.sharedInstance.requestConfiguration.testDeviceIdentifiers = @[admobTestDevice];
		}
        [bannerViewSingleton loadRequest: request];
    }
}

-(void) HideBanner
{
    if(bannerViewSingleton != NULL)
    {
        bannerViewSingleton.hidden = TRUE;
    }
}

-(void) IsBannerReady:(NSMutableDictionary*)result
{
    NSNumber *boolNumber = [NSNumber numberWithBool:isAdMobBannerReady];
    [result setValue:boolNumber forKey:@"RetValue"];
}

-(void) ShowInterstitialAds:(UIViewController*) viewController
{
    if (isAdMobInsterstitalReady)
    {
        [instertitialViewSingleton presentFromRootViewController:viewController];
        isAdMobInsterstitalReady = FALSE;
    }
}

-(void) IsInterstitalReady:(NSMutableDictionary*)result
{
    NSNumber *boolNumber = [NSNumber numberWithBool:isAdMobInsterstitalReady];
    [result setValue:boolNumber forKey:@"RetValue"];
}

-(void) Play:(UIViewController*) viewController
{
    //if ([[GADRewardBasedVideoAd sharedInstance] isReady])
    if(isAdMobVideoPlayable)
    {
        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:viewController];
        isAdMobVideoPlayable = FALSE;
    }
}

-(void) IsPlayable:(NSMutableDictionary*)result
{
    NSNumber *boolNumber = [NSNumber numberWithBool:isAdMobVideoPlayable];
    [result setValue:boolNumber forKey:@"RetValue"];
}

@end


