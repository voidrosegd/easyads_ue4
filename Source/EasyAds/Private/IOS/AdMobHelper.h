//
//  AdMobHelper.h
//  AdsUtil
//
//  Created by Xufei Wu on 2017/6/28.
//  Copyright © 2017年 Xufei Wu. All rights reserved.
//

#ifndef AdMobHelper_h
#define AdMobHelper_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AdMobHelper : NSObject

+ (AdMobHelper*) GetDelegate;
- (void) InitSDK:(UIViewController*) viewController AppID:(NSString*)AppID  testDevice:(NSString*)testDevice BannerUnit:(NSString*)BannerUnit InterstitalUnit:(NSString*)InterstitalUnit RewardedUnit:(NSString*)RewardedUnit callback:(void(*)(NSString* strType, int aomount)) callback rewardClose:(void(*)()) rewardClose interstitialShow:(void(*)()) interstitialShow  interstitialClick:(void(*)()) interstitialClick interstitialClose:(void(*)()) interstitialClose rcvDebugMessage: (void(*)(NSString* debugMessage)) rcvDebugMessage;

-(void) ShowBanner:(UIViewController*) viewController isBottom:(BOOL)isBottom;
-(void) HideBanner;
-(void) IsBannerReady:(NSMutableDictionary*)result;
-(void) ShowInterstitialAds:(UIViewController*) viewController;
-(void) IsInterstitalReady:(NSMutableDictionary*)result;
-(void) Play:(UIViewController*) viewController;
-(void) IsPlayable:(NSMutableDictionary*)result;

@end

#endif /* AdMobHelper_h */
