/*
* EasyAds - unreal engine 4 ads plugin
*
* Copyright (C) 2019 feixuwu <feixuwu@outlook.com> All Rights Reserved.
*/

#ifndef HttpHelper_h
#define HttpHelper_h

#import <UIKit/UIKit.h>

@interface HttpHelper : NSObject

-(void) requestValid:(NSString*) adPlatform cb:(void (^)(BOOL ret, NSDictionary* json) )cb;

@end


#endif /* HttpHelper_h */
