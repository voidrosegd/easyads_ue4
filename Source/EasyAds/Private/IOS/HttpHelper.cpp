//
//  HttpHelper.m
//  testAds
//
//  Created by Xufei Wu on 2017/6/30.
/*
* EasyAds - unreal engine 4 ads plugin
*
* Copyright (C) 2019 feixuwu <feixuwu@outlook.com> All Rights Reserved.
*/

#import <Foundation/Foundation.h>
#import "HttpHelper.h"

@implementation HttpHelper

-(void) requestValid:(NSString*) adPlatform cb:(void (^)(BOOL ret, NSDictionary* json) )cb
{
    NSURLSessionConfiguration *defaultConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *sessionWithoutADelegate = [NSURLSession sessionWithConfiguration:defaultConfiguration];
    
    NSString* strPackageName = [[NSBundle mainBundle] bundleIdentifier];
    
    NSString* strUrl = [NSString stringWithFormat:@"https://adsshare.org/valid?packageName=%@&adplatform=%@&os=IOS&market=1", strPackageName, adPlatform];
    
    NSURL *url = [NSURL URLWithString:strUrl];
    
    
    
    [[sessionWithoutADelegate dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error != nil)
        {
            NSLog(@"%@ request valid fail", adPlatform);
			dispatch_async(dispatch_get_main_queue(), ^{
				if (cb != nil)
				{
					cb(false, nil);
				}
			});
            return;
        }
        
        NSError* jsonerror;
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:data
                              options:kNilOptions
                              error:&jsonerror];
        
        if(json != nil)
        {
            NSNumber* ret = (NSNumber*)[json objectForKey:@"ret"];
            NSLog(@"%@ ret:%d",adPlatform, [ret intValue]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if(cb != nil)
                {
                    cb([ret intValue]==0, json);
                }
            });
        }
		else
		{
			dispatch_async(dispatch_get_main_queue(), ^{
				if (cb != nil)
				{
					cb(false, nil);
				}
				});
		}
        
    }] resume];
}

@end
